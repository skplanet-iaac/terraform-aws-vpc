variable "name" {
  type = "string"
  description = "the name of your stack, e.g. \"demo\""
}

variable "environment" {
  type = "string"
  description = "the name of your environment, e.g. \"prod\""
}

variable "cidr" {
  type = "string"
  description = "The CIDR block for the VPC."
}

variable "public_subnets" {
  type = "list"
  description = "List of public subnets"
}

variable "private_subnets" {
  type = "list"
  description = "List of private subnets"
}

variable "availability_zones" {
  type = "list"
  description = "List of availability zones"
}
